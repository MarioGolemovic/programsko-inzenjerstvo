import http.client

conn = http.client.HTTPSConnection("api-football-v1.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "fb304bfb41msh914df3b751102dbp1643cdjsn194d1987b961",
    'X-RapidAPI-Host': "api-football-v1.p.rapidapi.com"
    }

conn.request("GET", "/v3/timezone", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))