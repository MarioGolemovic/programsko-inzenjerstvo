import http.client

conn = http.client.HTTPSConnection("vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "fb304bfb41msh914df3b751102dbp1643cdjsn194d1987b961",
    'X-RapidAPI-Host': "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
    }

conn.request("GET", "/api/npm-covid-data/", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))