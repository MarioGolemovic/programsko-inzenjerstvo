import http.client

conn = http.client.HTTPSConnection("love-calculator.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "fb304bfb41msh914df3b751102dbp1643cdjsn194d1987b961",
    'X-RapidAPI-Host': "love-calculator.p.rapidapi.com"
    }

conn.request("GET", "/getPercentage?sname=Alice&fname=John", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))